#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
using namespace std;

#include <math.h>

#define M_PI (3.14159265358979323846264338327950288)

#include "cells.h"

int cell0_triangulazation(params* pars, tcell* cell){

  (*cell).border = (int*)malloc(sizeof(int)*(*cell).Nborder);

  double R = 1.0;

  (*cell).Nnodes = (*cell).Nborder;

  (*cell).nodes = (double*)malloc(2*sizeof(double)*(*cell).Nnodes);


  for( int i = 0; i < (*cell).Nborder; i++ ){
    (*cell).border[i] = i;
    (*cell).nodes[i*2] = R*sin(2*M_PI/(*cell).Nborder*i);
    (*cell).nodes[i*2+1] = R*cos(2*M_PI/(*cell).Nborder*i);
  }


  return 0;

}



int init_params(params* pars, tcell* cell){

  (*cell).Nborder = 20;



  if ( TEXTOUT >= 3 ) {
    cout << "start of init" << endl;
  }

  cell0_triangulazation(pars, cell);

  (*cell).f_at_nodes = (double*)malloc(2*sizeof(double)*(*cell).Nnodes);

  for( int i = 0; i < (*cell).Nnodes; i++ ){
    (*cell).f_at_nodes[i*2] = (*cell).nodes[i*2];
    (*cell).f_at_nodes[i*2+1] = (*cell).nodes[i*2+1];
  }


  (*pars).Nsteps = 2;
  (*pars).step = 0;

  (*pars).t = 0.0; // time
  (*pars).dt = 0.1; // time interval


  (*pars).delta = 1.0; // material constant
  (*pars).gamma = 1.0; // material constant
  (*pars).kd = 1.0; // depolymerization coifficient
  (*pars).alpha = 1.0; // intensity of the filament growth
  (*pars).D = 1.0; // diffusivity
  (*pars).konn = 0.0; // dissipation attachment rate coefficient
  (*pars).koff = 0.0; // dissipation detachment rate coefficient


  if ( TEXTOUT >= 3 ) {
    cout << "end of init" << endl;
  }

  return 0;

}


int print_params(params pars, tcell cell){

  cout << endl << "current parameters:" << endl;
  cout << "Nborder = " << cell.Nborder << endl;
  cout << "Nsteps = " << pars.Nsteps << endl;

  cout << pars.delta_name << " = " << pars.delta << " - " << pars.delta_disc << endl;
  cout << pars.gamma_name << " = " << pars.gamma << " - " << pars.gamma_disc << endl;
  cout << pars.kd_name << " = " << pars.kd << " - " << pars.kd_disc << endl;
  cout << pars.alpha_name << " = " << pars.alpha << " - " << pars.alpha_disc << endl;
  cout << pars.D_name << " = " << pars.D << " - " << pars.D_disc << endl;
  cout << pars.konn_name << " = " << pars.konn << " - " << pars.konn_disc << endl;
  cout << pars.koff_name << " = " << pars.koff << " - " << pars.koff_disc << endl;

  cout << endl;

  return 0;

}


int process_cell_oneStep(params* pars, tcell cell){

  double vx = 0.1;
  double vy = 0.13;

  for( int i = 0; i < cell.Nborder; i++ ){
    cell.f_at_nodes[i*2] += vx;
    cell.f_at_nodes[i*2+1] += vy;
  }

  (*pars).step = (*pars).step + 1;

  return 0;
}

string inttostr(int n){

  //char* buf;

  //std::sprintf(buf, "%d", n);

  string str; // = string(n);

  if (n < 10 ){
    str = "0" + to_string(n);
  }

  return str;

}


int process_cell(params pars, tcell cell){

  output_border_to_txt( pars, cell, inttostr(0) );

  for( int s = 0; s < pars.Nsteps; s++ ){

    cout << "step " << s + 1 << endl;;

    process_cell_oneStep( &pars, cell );

    output_border_to_txt( pars, cell, inttostr(s+1) );

  }

  return 0;

}

int output_border_to_txt(params pars, tcell cell, string fnum){

  if ( TEXTOUT >= 3 ) {
    //cout << "start of output to TXT" << endl;
  }

  if( FILEOUT >= 1){

    ofstream myfile1;

    cout << "step in params = " << pars.step << endl;;

	myfile1.open( string("border") + fnum + string(".txt") );
    //myfile1.open( "border00.txt" );

    for( int i = 0; i < cell.Nborder; i++ ){

      //myfile1 << cell.border[i*2] << " " << cell.border[i*2+1] << endl;

      myfile1 << cell.f_at_nodes[cell.border[i]*2] << " " << cell.f_at_nodes[cell.border[i]*2 + 1] << endl;

    }

    myfile1 << cell.f_at_nodes[cell.border[0]*2] << " " << cell.f_at_nodes[cell.border[0]*2 + 1] << endl;

    myfile1.close();

  }

  if ( TEXTOUT >= 2 ) {
//		output_data(exps[1]);
  }

  if ( TEXTOUT >= 3 ) {
    //cout << "end of output to TXT" << endl;
  }

  return 0;

}



int calc_current(){

  params pars;
  tcell cell;

  init_params( &pars, &cell );

  print_params( pars, cell );

  process_cell( pars, cell );

  //output_border_to_txt( pars, cell, "" );

  return 0;

}


int cells_run(int argc, char **argv){

	//using namespace std;

	int myrank = 0, np = 1;

	calc_current();

	return 0;

}
