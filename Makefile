CC = c++
#CC = x86_64-w64-mingw32-c++

MPIINCLUDE=-I/usr/include/mpich

MKLINCLUDE=-I../Program Files (x86)/Intel/oneAPI/mkl/2021.3.0/include

#GSLINCLUDE=-I/usr/include/
GSLINCLUDE1=-I/nfs/hybrilit.jinr.ru/user/k/knyazkov/soft/include
GSLINCLUDE2=-I/usr/include
GSLINCLUDE3=-I/home4/pstorage1/ipmex4/include

#CFLAGS = $(MPIINCLUDE) -fopenmp
CFLAGS = $(GSLINCLUDE1) $(GSLINCLUDE2) $(GSLINCLUDE3) $(MKLINCLUDE) -std=c++11

# Hydra
GSLLIBS1 = -L/nfs/hybrilit.jinr.ru/user/k/knyazkov/soft/lib
# Lenovo Cygwin
GSLLIBS2 = -L/lib
# JSCC
GSLLIBS3 = -L/home4/pstorage1/ipmex4/lib
GSLLIBS4 = ''

LFLAGS = $(GSLLIBS1) $(GSLLIBS2) $(GSLLIBS3)

EXETARGET = tuvp2vr.exe

# Lenovo Laptop Cygwin
#PROJ_PATH = /home/User/work/dpproj

# Hydra
#PROJ_PATH = ~/work/dpproj

# Ubuntu
#PROJ_PATH = ~/work/dpproj

# LENOVO-PC
#PROJ_PATH = ~/work/dpproj_new/dpproj

# MSC
PROJ_PATH = .

SRC_PATH = $(PROJ_PATH)/src
BIN_PATH = $(PROJ_PATH)/bin
OUT_PATH = $(PROJ_PATH)/output
POSTPROC_PATH = $(PROJ_PATH)/postprocessing
SCRIPTS_PATH = $(SRC_PATH)/scripts

PLOTLAYERSCRIPT = plotlayer1.txt

PLOTCELLSCRIPT = plotcell1.inp

#
# Video encoder:
#   avconv - for linux
#   ffmpeg.exe - for windows
#
AVICONV = ffmpeg.exe
AVIRATE = 3
AVIOPT = -framerate 25 -r $(AVIRATE) -i

.PHONY: plots, clean, clobber


run: build
	./rs


build: $(BIN_PATH)/run $(SRC_PATH)/main.cpp $(SRC_PATH)/cells.cpp $(SRC_PATH)/cells.h

$(BIN_PATH)/main.o : $(SRC_PATH)/main.cpp
	$(CC) $(CFLAGS) -c $(SRC_PATH)/main.cpp -o $(BIN_PATH)/main.o

$(BIN_PATH)/cells.o : $(SRC_PATH)/cells.cpp $(SRC_PATH)/cells.h
	$(CC) $(CFLAGS) -c $(SRC_PATH)/cells.cpp -o $(BIN_PATH)/cells.o

$(BIN_PATH)/run: $(BIN_PATH)/main.o $(BIN_PATH)/cells.o
	$(CC) $(LFLAGS) $(BIN_PATH)/main.o $(BIN_PATH)/cells.o -o $(BIN_PATH)/run -lm


plots: run $(SCRIPTS_PATH)/$(PLOTCELLSCRIPT)
	@echo
	@echo Plotting results...
	./ps


runall: $(OUT_PATH)/executed
	@echo

#	./rs
#	> $(OUT_PATH)/executed
#	$(BIN_PATH)/run

$(OUT_PATH)/executed: $(BIN_PATH)/run ./input.txt
	./rs
	> $(OUT_PATH)/executed



builddll: $(BIN_PATH)/dpproj.dll

##$(BIN_PATH)/dpprojmno.o : $(SRC_PATH)/dpprojmno.cpp $(SRC_PATH)/dpprojmno.h
##	$(CC) $(CFLAGS) -c -BUILDING_DPPROJ_DLL $(SRC_PATH)/dpprojmno.cpp -o $(BIN_PATH)/dpprojmno.o

##$(BIN_PATH)/dpproj.o : $(SRC_PATH)/dpproj.cpp $(SRC_PATH)/dpproj.h
##	$(CC) $(CFLAGS) -c -BUILDING_DPPROJ_DLL $(SRC_PATH)/dpproj.cpp -o $(BIN_PATH)/dpproj.o

$(BIN_PATH)/dpproj.dll: $(BIN_PATH)/dpproj.o $(BIN_PATH)/dpprojmno.o
	$(CC) $(LFLAGS) $(BIN_PATH)/dpproj.o $(BIN_PATH)/dpprojmno.o -shared -o $(BIN_PATH)/dpproj.dll  -lgsl -lgslcblas -lm


#/////

deploydll: builddll
	cp -f $(BIN_PATH)/dpproj.dll ../dpproj-calcs/dlls/dpproj.dll
	cp -f $(BIN_PATH)/dpproj.dll ../sputnik/dlls/dpproj.dll
	cp -f $(SRC_PATH)/dpproj.h ../sputnik/ConsoleApplication2/ConsoleApplication2/dpproj.h


plotlayer:  $(POSTPROC_PATH)/exp0_figlayer.png
	@echo

$(POSTPROC_PATH)/exp0_figlayer.png: $(OUT_PATH)/exp0_epsilon.txt $(POSTPROC_PATH)/scripts/$(PLOTLAYERSCRIPT)
	cp $(OUT_PATH)/exp0_epsilon.txt $(POSTPROC_PATH)/exp0_epsilon.txt
	cp $(POSTPROC_PATH)/scripts/$(PLOTLAYERSCRIPT) $(POSTPROC_PATH)/plotlayerscript.txt
	./rs-layer
	#gnuplot ./plotlayerscript.txt

$(OUT_PATH)/exp0_epsilon.txt: $(OUT_PATH)/executed
	@echo


#$(PROJ_PATH)/exp0_figlayer.png: $(PROJ_PATH)/exp0_epsilon.txt
#	gnuplot -e "expname='exp0'" plotlayer

$(PROJ_PATH)/exp1_figlayer.png: $(PROJ_PATH)/exp1_epsilon.txt
	gnuplot -e "expname='exp1'" plotlayer

$(PROJ_PATH)/exp0_epsilon.txt: $(BIN_PATH)/run $(PROJ_PATH)/input.txt
	./rs

$(PROJ_PATH)/exp1_epsilon.txt: $(BIN_PATH)/run $(PROJ_PATH)/input.txt
	./rs

#$(PROJ_PATH)/exp*_figlayer.png: $(PROJ_PATH)/exp*_epsilon.txt
#	gnuplot $(inputs) plotlayer

plotlayers:  $(PROJ_PATH)/exp0_figlayer.png $(PROJ_PATH)/exp1_figlayer.png


#exp*_figlayer.png: $(PROJ_PATH)/exp*_epsilon.txt
#	gnuplot $(input) -o $(output)


webplot: B0_abs.txt B0_arg.txt plotscriptweb
	@echo
	@echo Plotting web picture...
	gnuplot plotscriptweb

kosmonitfig1: KOSMONIT_figDefFromAlpha_def.txt run plotdeffromalpha
	@echo
	@echo Plotting web picture...
	gnuplot plotdeffromalpha


exp18_10_01-fig1:  $(POSTPROC_PATH)/exp_R0fromA1.png
	@echo

$(POSTPROC_PATH)/exp_R0fromA1.png: $(OUT_PATH)/output_R0-from-A1.txt $(POSTPROC_PATH)/scripts/plotscript_R0FromA1.txt
	cp $(OUT_PATH)/output_R0-from-A1.txt $(POSTPROC_PATH)/output_R0-from-A1.txt
	cp $(POSTPROC_PATH)/scripts/plotscript_R0FromA1.txt $(POSTPROC_PATH)/plotscript_R0FromA1.txt
	./rs-R0fromA1
	#gnuplot ./plotlayerscript.txt

$(OUT_PATH)/output_R0-from-A1.txt: $(OUT_PATH)/executed
	@echo


tuvp2vr.exe: ./Release/tuvp2vr.exe
	rm -f tuvp2vr.exe
	cp ./Release/tuvp2vr.exe ./

solution.txt: $(EXETARGET)
	@echo
	@echo Running code...
	rm -f step*.txt
	tuvp2vr.exe
	cat ha > solution.txt

plots.png: solution.txt plot_solution.py
	@echo
	@echo Plotting results...
	rm -f plot*.png
	rm -f centerline*.png
	python plot_solution.py
	cat ha > plots.png

#
# copy from animate target:
#
#python animate_solution.py
#avconv -i plot_r%d.png solution_r.avi
#avconv -i plot_r_mpi%d.png solution_r_mpi.avi
#avconv -i plocm_I1%d.png solution_I1.avi
#avconv -i plocm_sigma_e%d.png solution_sigma_e.avi
#avconv -f image2 -i plotcm_I1%d.png -vcodec h264 -crf 1 -r 24 out.mov
#avconv -i plotcm_I1%d.png -s solution_I1.avi
#avconv -i plotcm_sigma_e%d.png solution_sigma_e.avi


animate: solution_T0.avi solution_I1.avi solution_taup_rr_t.avi solution_sigma_e.avi solution_S.avi solution_T0_t.avi
	@echo
	@echo Animating results...
#	rm -f *.avi

solution_T0.avi: plots.png
	rm -f solution_T0.avi
	$(AVICONV) $(AVIOPT) plotcm_T0%d.png solution_T0.avi

solution_I1.avi: plots.png
	rm -f solution_I1.avi
	$(AVICONV) $(AVIOPT) plotcm_I1%d.png solution_I1.avi

solution_taup_rr_t.avi:	plots.png
	rm -f solution_taup_rr_t.avi
	$(AVICONV) $(AVIOPT) plotcm_taup_rr_t%d.png solution_taup_rr_t.avi

solution_sigma_e.avi: plots.png
	rm -f solution_sigma_e.avi
	$(AVICONV) $(AVIOPT) plotcm_Sigma_e%d.png solution_sigma_e.avi

solution_T0_t.avi: plots.png
	rm -f solution_T0_t.avi
	$(AVICONV) $(AVIOPT) plotcm_T0_t%d.png solution_T0_t.avi

solution_S.avi:	plots.png
	rm -f solution_S.avi
	$(AVICONV) $(AVIOPT) plotcm_S%d.png solution_S.avi

clean:
	rm -f $(BIN_PATH)/*.o $(BIN_PATH)/run $(BIN_PATH)/*.dll $(OUT_PATH)/run

clobber: clean
	rm -f solution.txt *.png

resclean:
	rm -f $(OUT_PATH)/*.txt
	rm -f $(OUT_PATH)/executed
	rm -f $(OUT_PATH)/output.txt
	rm -f $(OUT_PATH)/*.png $(OUT_PATH)/B*.txt $(OUT_PATH)/C*.txt $(OUT_PATH)/fig*.txt $(OUT_PATH)/*.out $(OUT_PATH)/*.avi $(OUT_PATH)/solution.txt $(OUT_PATH)/plots $(OUT_PATH)/Tmax*.txt $(OUT_PATH)/input.txt $(OUT_PATH)/output.txt
	rm -f $(OUT_PATH)/run.*/*
	rm -f $(OUT_PATH)/run.*/.hosts
	rm -f $(OUT_PATH)/plotscript
	rm -f $(OUT_PATH)/*
	rmdir $(OUT_PATH)/run.*


ppclean:
	rm -f $(POSTPROC_PATH)/*.txt
	rm -f $(POSTPROC_PATH)/*.png

imgclean:
	rm -f $(OUT_PATH)/*.png

allclean: ppclean clean resclean
	rm *~ *dump ./set
