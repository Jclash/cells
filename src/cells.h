
#include "mkl.h"

// TEXTOUT
// -1 - no output to stdou at all
// 0 - prints only total time
// 1 - output to stdout of system (basic) information
// 2 - output to stdout of the whole workflow
// 3 - output to stdout of the whole workflow and debug text
#define TEXTOUT 3

// FILEOUT
// 0 - no file output
// 1 - basic file output
// 2 - extended file output
// 3 - debug file output
#define FILEOUT 1


struct params{

	int Nsteps;
	int step;

	double t; // time
	double dt; // time interval

	double delta; // material constant
	string delta_name = "delta";
	string delta_disc = "material constant";

	double gamma; // material constant delta
    string gamma_name = "gamma";
	string gamma_disc = "material constant";

	double kd; // depolymerization coifficient
    string kd_name = "kd";
	string kd_disc = "depolymerization coifficient";

	double alpha; // intensity of the filament growth
    string alpha_name = "alpha";
	string alpha_disc = "intensity of the filament growth";

	double D; // diffusivity
    string D_name = "D";
	string D_disc = "diffusivity";

	double konn; // dissipation attachment rate coefficient
    string konn_name = "k_onn";
	string konn_disc = "dissipation attachment rate coefficient";

	double koff; // dissipation detachment rate coefficient
    string koff_name = "k_off";
	string koff_disc = "dissipation detachment rate coefficient";


};


struct tcell{
	double* nodes; // D
	int* border; //
	double* f_at_nodes;
	int Nborder;
	int Nnodes;

	double* p; // pressure
	double* u; // velocity field
	double* c; // density of Arpin
	double* mu; // density of growth factors on the boarder
	double* dOmega; // boarder of the domain

	// derived
	double* Vnu; // normal velocity of the boarder
	double* k; // curvature of the boarder

};


int cells_run(int, char**);


int output_border_to_txt(params, tcell, string);
